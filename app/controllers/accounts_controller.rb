class AccountsController < ApplicationController

  expose(:accounts) do
    Message.select(:android_id).map(&:android_id).compact.uniq
  end

  def index
  end
end
