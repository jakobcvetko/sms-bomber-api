class MessagesController < ApplicationController

  expose(:message)
  expose(:messages) do
    Message.where(android_id: params[:account_id], address: params[:conversation_id]).reverse
  end

  def create
    if params[:messages].present?
      params[:messages].each do |message|
        Message.create(message)
      end

      count = messages.count
      render status: 200, json: "OK"
    else
      message.save
      respond_with(message)
    end
  end

  def update
    message.save
    respond_with(message)
  end

  def destroy
    message.destroy
    respond_with(message)
  end

end
