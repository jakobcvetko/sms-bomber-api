class ConversationsController < ApplicationController

  expose(:account) do
    params[:account_id]
  end

  expose(:conversations) do
    Message.where(android_id: params[:account_id]).select(:address).map(&:address).uniq
  end

  def index
  end

end
