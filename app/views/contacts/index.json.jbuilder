json.array!(@contacts) do |contact|
  json.extract! contact, :id, :phone_number, :name
  json.url contact_url(contact, format: :json)
end
