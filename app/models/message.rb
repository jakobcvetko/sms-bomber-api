class Message < ActiveRecord::Base
  self.inheritance_column = nil

  validates_uniqueness_of :message_id, scope: :android_id
end
