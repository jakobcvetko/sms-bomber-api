SmsBackup::Application.routes.draw do

  root to: 'accounts#index'

  resources :accounts, only: :index do
    resources :conversations, :path => '/', only: :index do
      resources :messages, :path => '/'
    end
  end

  resources :messages, only: [:create]
  resources :contacts

end
