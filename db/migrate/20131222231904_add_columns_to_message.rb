class AddColumnsToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :type, :integer
    add_column :messages, :sent_at, :datetime
    add_column :messages, :message_id, :integer
    add_column :messages, :android_id, :string
  end
end
