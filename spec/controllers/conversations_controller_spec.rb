require "spec_helper"

describe ConversationsController do
  let(:account) { Message.create(android_id: "id", message_id: "id").android_id  }
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index, account_id: account
      expect(response).to be_success
      expect(response.status).to eq(200)
      expect(response).to render_template("index")
    end

  end
end
